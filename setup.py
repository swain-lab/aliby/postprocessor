from setuptools import find_packages, setup

setup(
    name="aliby-post",
    version="0.1",
    packages=find_packages(),
    # package_dir={"": "postprocessor"},
    # include_package_data=True,
    url="https://git.ecdf.ed.ac.uk/amuoz/postprocessing",
    license="MIT",
    author="Swain Lab",
    author_email="alan.munoz@ed.ac.uk",
    description=(
        "Processing extracted data from segmented images of budding yeast"
    ),
    python_requires=">=3.7",
    install_requires=[
        "numpy",
        "scipy",
        "scikit-image",
        "imageio",
        "more-itertools",
        "matplotlib",
        "seaborn",
        "tqdm",
        "more_itertools",
        "catch22",
        "leidenalg",
        # "agora@git+ssh://git@git.ecdf.ed.ac.uk/swain-lab/python-pipeline/agora",
    ],
)
