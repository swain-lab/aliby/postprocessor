# Post-processing pipeline

Module to analyse extractions results. This does not deal with images, only with extracted data from images and metadata saved in an hdf5 file.

## Installation
There are multiple ways to install it, based on the use-case.

1. Postprocessing tools only, for people who just want to analyse data and/or plot using local files:

```bash
pip install aliby-post
```

2. Entire aliby pipeline, to run the whole pipeline and access networking tools:

```bash
pip install aliby
```

3. For development, via poetry:

```bash
git clone git@git.ecdf.ed.ac.uk:swain-lab/aliby/postprocessor.git
cd postprocessor
poetry install -vv
```

## Design
The main two classes in this package are "Parameters" and "Processes". Parameters make the data analysis replicable, every process must have an associated Parameters class (Inheriting from the Abstract Base Class -or ABC - ParametersABC). This ABC class contains functions to save and read parameters.

The front-end is PostProcessor, which uses agora's Signal and Writer classes for IO. It dynamically loads postprocesses located in "postprocessor.core.processes.PROCESS_NAME".

## Development guidelines
- If you require images for your new function, it should go into the extraction package; if you require plotting for more than debugging, your functions should go in TODO:resolve this location.
- Similar to extraction, the processing pipeline must be defined beforehand as a parameter class; we could add live testing in the future, using logging to recover the sequence of functions used.
- Merging and Picking cells are done before any other process for consistency.

## Development focus
"Process" in the context of this package means a procedure applied to a pandas dataframe with a set of parameters.

 * Filter
   - [X] Time filter?
 * Single channel
   - Does not require metadata
     + [ ] Normalise using timepoints
     + [X] Fourier series
     + [X] Calculate change over time (e.g. growth rate)
     + [X] Savitsky-Golay (with limitations)
   - Require cell_info data
     + [X] Daughter volume
 * Compare two time series
   - General
     + [ ] Cross-correlation
     + [ ] Group cell values by time or lineage
   - Single cell
     + [X] Calculate avg/max growthrate
     + [X] fluorescence
   - Mother-daughter relationship
     + [X] Growth
     + [X] Fluorescence
     + [X] NucLoc
     + [ ] Compare time between divisions

 This is moving to the Compiler

 * Split timelapse in subsections using env condition (metadata)


##  Using Processes as functions

You can also use the Processes class as a normal method, it loads its own default parameters, overrides in case you pass additional arguments, and applies the process to a given dataset. Here is an example of running Savitzky-Golay.

```python
import numpy as np
import pandas as pd

from postprocessor.core.processes.savgol import savgol

data = pd.DataFrame(np.random.randint(100, size=(100,100)))

print(data)

print(savgol.as_function(data, window=5))
```

## Troubleshooting

Just open an issue and I'll have a look ASAP.
